<?php

return [
    'home'=>'Inicio',
    'search' => 'Buscar',
    'products' => 'Productos',
    'be' => 'Ser una distribución',
    'contact' => 'Contacto',
    'en' => 'Inglés',
    'es' => 'Español',
    'res' => 'resultados',
    're' => 'Resultado',
    'read' => 'Lee mas',
    'related' => 'Producto relacionado:',
]; 
