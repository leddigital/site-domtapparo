<?php

return [
    'title'=>'Entre en contacto',
    'name' => 'Nombre',
    'email' => 'Email',
    'company' => 'Empresa',
    'subject' => 'Tema',
    'message' => 'Mensaje',
    'send'=>'Enviar'
];
