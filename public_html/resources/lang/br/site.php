<?php

return [
    'home'=>'Página Inicial',
    'search' => 'Pesquisar',
    'products' => 'Produtos',
    'distribution' => 'Seja um distribuidor',
    'contact' => 'Contato'
];
