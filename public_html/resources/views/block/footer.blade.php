<footer class="page-footer font-small blue">
    <div class="footer" style="background: #cbb36d;">
        <div class="footer-copyright text-center py-3" style="color:#000;">© 2019 DomTapparo:
            <a href="http://agencialed.com.br/" target="_blank" style="color: #000;">Developed by Led Digital</a>
        </div>
     </div>
</footer>
