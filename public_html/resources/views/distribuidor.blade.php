@extends('layouts.default')

@section('content')
<section>
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-6 col-12">
                    <h3 class="title h4">
                        @lang('distribuitor.be')
                    </h3>
                    <p class="dist-desc h5">
                        @lang('distribuitor.by')
                    </p>
                    <ul class="ml-5 mt-4" id="list-desc-dist">
                        <li> @lang('distribuitor.40')</li>
                        <li> @lang('distribuitor.products')</li>
                        <li> @lang('distribuitor.presente')</li>
                        <li> @lang('distribuitor.2gold')</li>
                        <li> @lang('distribuitor.2conse')</li>
                        <li> @lang('distribuitor.reco')</li>
                    </ul>
                </div>
                <div class="inf col-lg-6 col-12">
                    <h3 class="title h4">@lang('distribuitor.ex')</h3>
                    <h6 class="lead mt-3 dist-info-name">Waelte Ferraz</h6>
                    <h6 class="title mt-3 dist-info">Raiz Brasil - DomTapparo</h6>
                    <div class="fotos">
                        <img class="w-25 my-2 mx-2" src="{{ asset('img/raiz_brasil-170x300.png')}}">
                        <img class="w-25 my-2 mx-2" src="{{ asset('img/waelte_rosto.png')}}">
                    </div>
                    <h6 class="text-info"><i class="fa fa-phone" aria-hidden="true"></i> +55 17 3212-7404</h6>
                    <a class="text-info" href="https://api.whatsapp.com/send?1=pt_BR&phone=5517996571818">
                        <i class="fa fa-whatsapp" aria-hidden="true"></i> +55 17 99657-1818
                    </a>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-lg-6 col-12">
                    <h4 class="dist-info-1 pb-3 "> @lang('distribuitor.form').</h4>
                <form class="m-2" action="{{ route('ajaxRequest',['locale'=>Config::Post('app.locale')])}}" method="POST">
                        <div class="name">
                            <label class="h6">@lang('distribuitor.nform')</label>
                            <div class="form-group">
                                <div class="form-row">
                                    <input required class="form-control" name="name" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="email">
                            <label class="h6">@lang('distribuitor.eform')</label>
                            <div class="form-group">
                                <div class="form-row">
                                    <input required class="form-control" name="email" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="company-name">
                            <label class="h6">@lang('distribuitor.cform')</label>
                            <div class="form-group">
                                <div class="form-row">
                                    <input required class="form-control" name="empresa" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="subject">
                            <label class="h6">@lang('distribuitor.sform')</label>
                            <div class="form-group">
                                <div class="form-row">
                                    <input required class="form-control" name="tema" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="mensage">
                            <label class="h6">@lang('distribuitor.mform')</label>
                            <div class="form-group">
                                <div class="form-row">
                                    <input required class="form-control" name="msg" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="button mt-2">
                            <button type="submit" style="color:#fff; cursor: pointer; padding: 6px 11px; border: none; border-radius: 5px; border-bottom: 2px solid #9e8740; background: #e0c880;">@lang('distribuitor.contact')</button>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6 col-12 mb-2 mt-4">
                    <h4 class="lead h6 mb-2 dist-info">@lang('distribuitor.name')</h4>
                <img class="w-100" src="{{ asset('img/Fachada-showeoon-baixa-1.jpeg') }}"></div>
            </div>
        </div>
    </section>
@endsection
