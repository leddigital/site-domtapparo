@extends('layouts.default')

@section('content')
<section>
    <div class="container">
        <div class="nav-head">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home', ['locale'=>Config::get('app.locale')]) }}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('category', ['locale'=>Config::get('app.locale'),'url'=>$categoria->url]) }}">Products</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{$product->name}}</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<section class="products">
    <div class="container">
        <div class="row" id="product-desc">
            <div class="col-lg-4 col-12">
                <img class="imgProd img-fluid" src="{{asset('img/produtos/'.$product->image)}}">
            </div>
            <div class="col-lg-4 col-12 product-info list-unstyled mt-5">
                <h4>{{$product->name}}</h4>
                <p>{{$product->description}}</p>
                <li><strong>ABV (Alcohol by volume):</strong> 0.15 </li>
                <li><strong>GR /Vol.:</strong> 500 ml</li>
                <li><strong>Bt /Ct:</strong> 12</li>
                <li><strong>Ct /Pt :</strong> 80</li>
                <li><strong>Ref.:</strong> X-20</li>
                <li class="mt-4">SKU: X-25-E Categories: <a href="#">Liqueur</a>, <a href="#">Tradicional</a> a Tags: <a href="#">Liqueur</a>,<a href="#">Tradicional</a> a Description</li>
            </div>
        </div>
        <hr>
        <div class="prod-related">
            <div class="tab-pane ml-4 mt-4 fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">@lang('site.related')</div>
            <div class="container">
                <div class="row">
                    @foreach ($product as $item)
                    <div class="prod-rel-link col-lg-3 col-6">                        
                        <a class="rel-link prod-link text-center" href="product.html">
                            <img class="img-produto img-fluid" src="{{ asset('img/produtos/'.$product->image) }}">
                            <h5>{{ $product->name }}</h5>
                        </a>                                      
                        <div class="button text-center">
                            <a href="javascript:;"><button class="btn-rm mb-5">@lang('site.read')</button></a>
                        </div>                        
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
