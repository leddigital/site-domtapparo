<!doctype html>
<html lang="{{  Config::get('app.locale')=='es'?'es-419':'en'  }}">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- JavaScript Padrão -->
    <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/Nivo-Slider-jQuery-master/jquery.nivo.slider.js')}}"></script>
    <script src="{{ asset('plugins/Nivo-Slider-jQuery-master/jquery.nivo.slider.pack.js')}}"></script>
    <script src="{{ asset('js/script.js') }}"></script>

    @yield('scripts')
    <!-- CSS PADRÃO -->
    <link href="{{ asset('css/bootstrap-reboot.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/Nivo-Slider-jQuery-master/nivo-slider.css') }}" rel="stylesheet" type="text/css">
    


<body>    
    @include('block.header')
    @yield('content')
    @include('block.footer')
</body>

</html>
